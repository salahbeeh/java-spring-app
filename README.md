<h1 style="text-align:center">Java App deployment on EC2 instance using Terraform and Gitlab CI/CD</h1>

_This app is orignally Cloned from [bezkoder](https://github.com/bezkoder/spring-boot-data-jpa-mysql)_

<p align="center">
<img src="./img/gitlab.png" alt="gitlab" width="220"/>
<img src="./img/terraform.png" alt="terraform" width="220"/>
</p>

<p align="center">
<img src="./img/spring.png" alt="spring" width="400"/>
</p>


This demo app shows a Java spring app deployed on AWS EC2 instance 
- Containered Java spring app and deployed on EC2 instance 
- connected to mysql server hosted on EC2 instance

## Overview of this Demo
---

The aim of this project is to help the development team deploy their application to the cloud in an organized, timely and automated manner.

The team is developing a java spring boot application connected to a MySQL database.

Development team repo https://github.com/bezkoder/spring-boot-data-jpa-mysql
The goal is to automate the deployment of this application to AWS servers.

## Get Started 
---
**My Approch**

1. Create an AWS EC2 machine using Terraform and install Docker.
  ```
  # AWS EC2 instace 
  resource "aws_instance" "app" {
    ami           = "ami-08d4ac5b634553e16"
    instance_type = "t2.micro"
    key_name = "simon"

    tags = {
      Name = "spring_app"
      Project = "Simon"
    }
  }
  ```
_don't forget to configure the provider as AWS_

### Install docker on the EC2 machine
- After (ssh)ing to the machine, (scp) the following script to the machine and run it or just run the following commands.
- For security purposes, pass the ssh privite key to gitlab environment variables as file.
``` sh
#! /bin/bash 

sudo apt update
sudo apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
apt-cache policy docker-ce
sudo apt install docker-ce
sudo systemctl status docker
```

2. Install MySQL database on the EC2 machine.
```sh
#! /bin/bash

sudo apt update
sudo apt upgrade
sudo apt install mysql-server
mysql --version
sudo systemctl status mysql
```

3. Clone the repo [https://github.com/bezkoder/spring-boot-data-jpa-mysql] in your
personal “GitLab” account.
``` 
hmm, you know how !
```
4. Create a “Dockerfile” for the application and push it to your personal Gitlab repository.
```Dockerfile
FROM eclipse-temurin:17-jdk-jammy

WORKDIR /app

COPY .mvn/ .mvn

COPY mvnw pom.xml ./

RUN ./mvnw dependency:go-offline

COPY src ./src

CMD ["./mvnw", "spring-boot:run"]
```

4. Configure the environment variable “DB_HOST” for the Database connection.
   - I didn't use RDS to cut the charges, I've configured the docker run -option [--network] to host, now I can explictly access the host network innterface. 
   - passed the Host as _localhost_ , as the host won't change anyway in my case I could've hard code it or pass it as env variable.
  

5. Create a Gitlab pipeline with 2 stages [build & deploy] to:
    - Build Dockerfile and push your image to the “Gitlab Container Registry”.
    - Deploy the image created to the EC2 machine you provisioned in Step 1.
      - [Note: The pipeline should automatically be triggered on the “main” branch
    only – other branches should not trigger the pipeline].

    ```yml
    stages:
     - build
     - deploy

   docker-build:
     # Use the official docker image.
     image: docker:latest
     stage: build
     services:
       - docker:dind
     before_script:
       - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
     # Default branch leaves tag empty (= latest tag)
     # All other branches are tagged with the escaped branch name (commit ref slug)
     script:
       - docker build --pull -t "$CI_REGISTRY_IMAGE${tag}" ./app
       - docker push "$CI_REGISTRY_IMAGE${tag}"
     # Run this job in a branch where a Dockerfile exists
     
     only:
       - master


   deploy:
     image: docker
     stage: deploy
     before_script:
       - 'command -v ssh-agent >/dev/null || ( apk add --update openssh )' 
       - eval $(ssh-agent -s)
       - echo "$AWS_KEY" | tr -d '\r' | ssh-add -
       - mkdir -p ~/.ssh
       - chmod 700 ~/.ssh
       - ssh-keyscan $AWS_URL >> ~/.ssh/known_hosts
       - chmod 644 ~/.ssh/known_hosts

     script:
       - apk add openssh-client
       - ssh -T -i ${PEM} ubuntu@"${AWS_URL}" "sudo docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY && sudo docker pull $CI_REGISTRY_IMAGE${tag} && sudo docker run -d --network host -p 8080:8080 -p 3306:3306 -e MYSQL_DB_HOST=localhost $CI_REGISTRY_IMAGE${tag}"
     only:
       - master 
     
## Testing my work
- using _postman_ let's try some POST, GET, PUT requests! 
<p align="center">
<img src="./img/postman.png" alt="postman"/>
</p>
looks fine to me! 

- Now let's try fetch them!
<p align="center">
<img src="./img/test.png" alt="test"/>
</p>

---

_you can find the Dockerfile withen /app folder_